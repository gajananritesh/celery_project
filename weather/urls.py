from django.conf.urls import url
from . import views

app_name='weather'
urlpatterns = [
url('^$', views.weather_view, name="weather"),
url('^delete/(?P<city>[-\w]+)/$', views.delete_view, name="delete")
]
# url(r'^delete/(?P<city>[-\w]+)/$', views.delete_view, name="delete")