import requests
from django.shortcuts import render,redirect, get_object_or_404, HttpResponse,HttpResponseRedirect
from weather.forms import CityForm
from django.http import HttpResponse
from .models import City

def weather_view(request):
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=ae3c68ddc7f437ecf3ff2cda351b34f0'
    
    if request.method == 'POST':
        form = CityForm(request.POST)
        city = request.POST.get('name')
        # if form.is_valid():
        #     city = form.cleaned_data['name']

        if not City.objects.filter(name=city).exists():
            form.save()
            #     return render(request, '/weather')
        else:
            form = CityForm()
            cities = City.objects.all().order_by('-time')

            weather_data = []

            for city in cities:
                r = requests.get(url.format(city)).json()

                city_weather = {
                    'city' : city.name,
                    'temperature' : r['main']['temp'],
                    'description' : r['weather'][0]['description'],
                    'icon' : r['weather'][0]['icon'],
                }
                weather_data.append(city_weather)
            return render(request, 'weather/weather.html', {'error_message':"City Name Exists, Enter a Different Name", 'form':form, 'weather_data' : weather_data,} )
            # return HttpResponse("City Name Exists, Enter a Different Name")

    form = CityForm()
    cities = City.objects.all().order_by('-time')

    weather_data = []

    for city in cities:
        r = requests.get(url.format(city)).json()

        city_weather = {
            'city' : city.name,
            'temperature' : r['main']['temp'],
            'description' : r['weather'][0]['description'],
            'icon' : r['weather'][0]['icon'],
        }
        weather_data.append(city_weather)

    context = {'weather_data' : weather_data, 'form':form}
    return render(request, 'weather/weather.html', context)

def delete_view(request, city):
    get_object_or_404(City, name=city).delete()
    # c = City.objects.filter(name=city)
    # c.delete()
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))