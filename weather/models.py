from django.db import models
from django.urls import reverse

class City(models.Model):
    name = models.CharField(max_length=50,unique='True')
    time = models.DateTimeField("Created on", auto_now_add=True)

    def __str__(self):
        return self.name

    class meta:
        verbose_name_plural = 'cities'
        ordering = ['-time', 'name']
    