from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string


def send_feedback_email(email, message):
    email2 = 'gajanan.ritesh@gmail.com'
    c = {'email': email, 'message': message,}

    email_subject = render_to_string(
        'feedback/email/feedback_email_subject.txt', c).replace('\n', '')
    email_body = render_to_string('feedback/email/feedback_email_body.txt', c)
    email_body2 = render_to_string('feedback/email/feedback_email_body2.txt', c)

    send_mail(
        email_subject, email_body2, settings.DEFAULT_FROM_EMAIL, [email],fail_silently=False
    )

    send_mail(
        email_subject, email_body, settings.DEFAULT_FROM_EMAIL, [email2],fail_silently=False
    )

