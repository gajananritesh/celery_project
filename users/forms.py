from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import CustomUser, TenthMark
from django.core.exceptions import ValidationError

class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('first_name','last_name','username','email','address','image') 

class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ('username','email')

class TenthMarkForm(forms.ModelForm):
    class Meta:
        model = TenthMark
        fields = '__all__'

    def clean_name(self):
        n = self.cleaned_data['name']
        if TenthMark.objects.filter(name=n).exists():
            raise ValidationError('Name already exist, Use another name.')
        return n

class GetNameForm(forms.Form):
    name = forms.CharField(label='Enter Name', max_length=20, id='search')

    def clean_name(self):
        na = self.cleaned_data['name']
        if not TenthMark.objects.filter(name=na).exists():
            raise ValidationError('Name doesnot exist in the database.')
        return na
