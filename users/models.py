from django.db import models
from django.contrib.auth.models import AbstractUser

class CustomUser(AbstractUser):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20, blank=True, null=True)
    email = models.EmailField()
    username = models.CharField(max_length=25, unique=True)
    password = models.CharField(max_length=100)
    address = models.CharField(max_length=50,blank=True, null=True)
    image = models.ImageField(height_field='image_height', width_field='image_width', max_length=None, blank=True, null=True)
    image_width = models.IntegerField(default=0)
    image_height = models.IntegerField(default=0)
    time = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.first_name

class TenthMark(models.Model):
    name = models.CharField(max_length=20)
    oriya = models.IntegerField()
    english = models.IntegerField()
    hin_san = models.IntegerField()
    math = models.IntegerField()
    science = models.IntegerField()
    history = models.IntegerField()
    geography = models.IntegerField()
    def __str__(self):
        return self.name