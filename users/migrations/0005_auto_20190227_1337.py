# Generated by Django 2.1.5 on 2019-02-27 08:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_customuser_file'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='file',
        ),
        migrations.AlterField(
            model_name='customuser',
            name='image',
            field=models.ImageField(blank=True, height_field='image_height', null=True, upload_to='media', width_field='image_width'),
        ),
    ]
