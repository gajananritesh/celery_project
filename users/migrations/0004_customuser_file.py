# Generated by Django 2.1.5 on 2019-02-27 07:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20190227_1202'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='file',
            field=models.FileField(blank=True, null=True, upload_to='images'),
        ),
    ]
