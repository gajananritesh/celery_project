from django.urls import reverse_lazy
from django.views import generic
from django.shortcuts import render
from .forms import CustomUserCreationForm, TenthMarkForm, GetNameForm
from .models import CustomUser, TenthMark
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login, get_user_model
import ipdb
from rest_framework.views import APIView
from rest_framework.response import Response
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

User = get_user_model()
class SignUp(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'users/signup.html'

def home_view(request):
    return render(request,'users/home.html')

def profile_view(request):
    if request.user.is_authenticated:
        u = request.user.username
        obj = CustomUser.objects.get(username=u)
        return render(request,'users/profile.html',{'obj':obj})
    else:
        return render(request,'users/home.html')

def chat_view(request):
    return render(request,'users/chart.html')

def chartjs_view(request):
    if request.method == 'POST':
        name = request.POST['name']
        name2 = "Results Of "+'"'+name+'"'
        if not TenthMark.objects.filter(name=name).exists():
            return render(request, 'users/chartjs.html',{'form2': GetNameForm(), 'error_msg': "Name doesnot exist in the database."})
        else:
            obj = TenthMark.objects.get(name=name)
            ori = obj.oriya
            eng = obj.english
            hin = obj.hin_san
            mat = obj.math
            sci = obj.science
            his = obj.history
            geo = obj.geography
            context = {'default': [ori, eng, hin, mat, sci, his, geo]}
            return render(request, 'users/chartjs.html', {'data': context, 'name':name2, 'form2': GetNameForm()})

    form2 = GetNameForm()
    return render(request, 'users/chartjs.html',{'form2':form2})

def get_data(request):
    data = {'sales':100,'customers':10,}
    return JsonResponse(data)

class ChartData(APIView):
    def get(self, request, format=None):
        labels = ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange']
        default_items = ['2','4','6','6','4','2']
        data = {
                'labels': labels,
                'default': default_items,
        }
        return Response(data)

def marks_view(request):
    if request.method == 'POST':
        form = TenthMarkForm(request.POST)
        if form.is_valid():
            form.save()
            form = TenthMarkForm()
            return render(request, 'users/add_mark.html',{'form':form, 'success_msg':"Data Saved Successfully..."})
        return render(request, 'users/add_mark.html',{'form': form,})
    else:
        form = TenthMarkForm()
        return render(request, 'users/add_mark.html',{'form':form})

def results_view(request):
    data = TenthMark.objects.all()

    paginator = Paginator(data, 10)
    page = request.GET.get('page')

    try:
        data = paginator.page(page)
    except PageNotAnInteger as e:
        print(e)
        data = paginator.page(1)
    except EmptyPage as e:
        print(e)
        data = paginator.page(paginator.num_pages)
    return render(request, 'users/results.html', {'data': data})

def eye_view(request):
    return render(request, 'users/eye.html')