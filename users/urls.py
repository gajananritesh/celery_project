from django.urls import path
from . import views
from .views import ChartData
urlpatterns = [
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('profile/', views.profile_view, name="profile"),
    path('chat/', views.chat_view, name="chat"),
    path('marks/', views.marks_view, name="marks"),
    path('results/', views.results_view, name="results"),
    path('chartjs/', views.chartjs_view, name="chartjs"),
    path('eye/', views.eye_view, name="eye"),
    path('api/', views.get_data, name="api"),
    path('api/data/', ChartData.as_view(), name="api-data"),
]