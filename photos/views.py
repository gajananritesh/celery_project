from django.views.generic.list import ListView
from django.shortcuts import redirect,get_object_or_404,HttpResponseRedirect,reverse
from photos.models import Photo
from feedback.forms import FeedbackForm


class PhotoView(ListView):
    model = Photo
    template_name = 'photos/photo_list.html'
    paginate_by = 9

    def get_context_data(self, **kwargs):
        context = super(PhotoView, self).get_context_data(**kwargs)
        context['form'] = FeedbackForm()
        return context

def DeleteView(request, id):
    get_object_or_404(Photo, id=id).delete()
    # return redirect('/')
    # return HttpResponseRedirect('home')
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))