from django.conf.urls import url
from .views import DeleteView,PhotoView

app_name='photos'
urlpatterns = [
url('^flicker/', PhotoView.as_view(), name="flicker"),
url('^delete/(?P<id>\d+)/$', DeleteView, name="delete")
]