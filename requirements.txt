amqp==1.4.9
anyjson==0.3.3
backcall==0.1.0
billiard==3.3.0.23
celery==3.1.15
colorama==0.4.1
decorator==4.3.2
dj-database-url==0.5.0
Django==2.1.5
django-heroku==0.3.1
djangorestframework==3.9.2
gunicorn==19.9.0
ipdb==0.11
ipython==7.2.0
ipython-genutils==0.2.0
jedi==0.13.2
kombu==3.0.37
parso==0.3.3
pickleshare==0.7.5
Pillow==5.4.1
prompt-toolkit==2.0.8
psycopg2==2.7.7
Pygments==2.3.1
pytz==2018.9
redis==2.10.5
requests==2.7.0
six==1.12.0
traitlets==4.3.2
vine==1.2.0
wcwidth==0.1.7
whitenoise==4.1.2
