from django.conf.urls import include, url,static
from django.contrib import admin
from django.views.generic.base import TemplateView
from feedback.views import FeedbackView
from weather.views import weather_view
from users.views import home_view
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', home_view, name="home"),
    url(r'^feedback', FeedbackView.as_view(), name="feedback"),
    url(r'^admin', admin.site.urls),
    url('users/', include('users.urls')),
    url('users/', include('django.contrib.auth.urls')),
    url(r'^photos/', include('photos.urls')),
    url(r'^weather/', include('weather.urls')),
    url(r'^blogs/', include('blogs.urls')),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

